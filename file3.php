
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="preconnect" href="https://fonts.gstatic.com"> 
	<link href="https://fonts.googleapis.com/css2?family=Acme&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-		    UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<style>
    h2{
        margin-bottom: 15px;
	  font-family:'Acme', sans-serif;
    }
    .error{
	position: absolute;
      color: red;
      top: 80px;
	left: 35%;
    }

    .trail{
	    border-radius: 30px;
    }

</style>
</head>
<body>
<div class="container-fluid">
	<nav class="navbar navbar-dark sticky-top py-0 rounded" style="background: #12232E; color: white">
	<div class="navbar-brand">
    <h2>RGUKT students Academic Knowledge Hub</h2></div>	
</nav>
</div>
<marquee style="padding-top: 10px"  onmouseover="this.stop();" onmouseout="this.start();">File size allowed upto 500MB</marquee>
<div class="container">
<div class="border border-muted mt-3 p-3 mx-auto mt-xl-5 trail" style="max-width: 400px;">
	<h2 class="text-center">Upload file</h2>
	<form action = "file3.php" method="post" enctype = "multipart/form-data">
	<div class="form-group">
		<label for="name">Select a file to upload:</label>
		<input type ="file" name ="file" id ="file" />
	</div>

	<div class="form-group">
		<label for="about">Description:</label><br>
		<input type ="text" name="about" id="about" placeholder="Enter about this file" />
    </div>
    <div class="form-group">
    <label for="category">Select Category:</label>
    <select id="category" name ="category">
  		<option value="general_books">General books</option>
		<option value="competitive">Competitive exams</option>
		<option value="p1">P1 content files</option>
		<option value="p2">P2 content files</option>
		<option value="e1">E1 content files</option>
		<option value="e2">E2 content files</option>
		<option value="e3">E3 content files</option>
		<option value="e4">E4 content files</option>
		<option value="others">Others</option>  
    </select>
    </div>
    <center><input type ="submit" name= "submit" value= "upload" class="btn" style="background: #12232E; color: white"/></center>
   
    </form>	
</div>
</div>

<?php

$connect = new mysqli("localhost", "root", "", "trail") or die("Unable to connect database");

if(isset($_POST['submit']))
{
	$name = $_FILES['file']['name'];
	$temp = $_FILES['file']['tmp_name'];
	$category = $_POST['category'];
    	$about = $_POST['about'];
	$size = $_FILE['file']['size'];
	
	if(!empty($name))
	{
	move_uploaded_file($temp,"files/".$name);
	$url = "files/".$name;

	$position= strpos($name, "."); 
	$fileextension= substr($name, $position + 1);
	$fileextension= strtolower($fileextension);
    	$types = array('odt','ppt','pdf','img','jpg','jpeg','doc','docx','txt','java','c','obj','py','html','xml','css','js','jquery');

	$sql="select * from files";
	$result = $connect->query($sql);
	$flag=0;
	if($result->num_rows>0)
	{
		while($row=$result->fetch_assoc())
		{
            if($name==$row['name'])
            {
				$flag=1;
				echo "<p class='error'>File with name " .$name." is already existed</p>";
				break;
            }
        
		}
	}

	if($flag!=1)
	{
	if(in_array($fileextension, $types))
	{
		if($size < 524288000)
		{
		$newrow="insert into files(name,url,category,about) values('$name', '$url','$category','$about')";
		if($connect->query($newrow))
		{
			echo '<p class="error text-success ">'.$name. " has been uploaded successfully</p>";
			?>
			<script>
			location.replace("file2.php");
			</script>
			<?php
		}
		else
			echo "Error in creating record:" .$connect->error;
		}
		else
			echo  "<p class='error'> File size ".$size. "is morethan 500MB</p>";
	}
	else
		echo "<p class='error'>".$fileextension. " type is not allowed</p>";
	}
	}

	else
		echo "<p class='error'>No file selected</p>";	

}

?>
</body>
</html>