<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="preconnect" href="https://fonts.gstatic.com"> 
	<link href="https://fonts.googleapis.com/css2?family=Acme&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-		    UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script type ="module" src="trail.js" defer></script>
<style>

    *{
        box-sizing: border-box;
    }
    #heading{
        color: white;
        font-family:'Acme', sans-serif;
    }

    body{
        background-color: #F8F8F8;
    }

    .subheading{
        color: #12232E;
    }

    .video{
        border : 2px solid #E0E0E0;
        padding : 10px;
        margin: 10px 10px 10px 10px;
    }


     .butt{
         margin-left: 50px;
         border: 1px solid #12232E;
     }

     .nav2{
         margin-top: 10px;
         
     }

     .logout{
            border: 1px solid white;
            border-radius: 5px;
        }

        .icon{
			color: white;
			margin: 5px;
			font-size: 35px;
			cursor: pointer;
		}


</style>
</head>
<body>
<div class="contanier-fluid">
<nav class="navbar navbar-expand-sm  navbar-dark sticky-top py-1 rounded nav " style=" border: 1px ridge gainsboro;background-color:rgba(0,0,0,0.9)">
	<h2 id="heading">RGUKT students Academic knowledge Hub</h2>
	<button class="navbar-toggler" data-toggle="collapse" data-target="#links">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="links">
		<ul class="navbar-nav ml-auto nav">
                  <li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white mt-1">Home</h5>-->
				<h5><a class="nav-link text-white mt-1" href="homepage.php">Home</a></h5>
			</li>
			<li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white mt-1"  onClick = "fetchFile('video1.php')">Videos</h5>-->
				<h5><a class="nav-link text-white mt-1" href="#">Videos</a></h5>
			</li>
			<li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white mt-1"  onClick = "fetchFile('file2.php')">Files</h5>-->
				<h5><a class="nav-link text-white mt-1" href="file2.php">Files</a></h5>
                  </li>
                  <li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white mt-1" onClick = "fetchFile('chat1.php')">Connect</h5>-->
				<h5><a class="nav-link text-white mt-1" href="ideas.php">Ideas</a></h5>
			</li>
                  <li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white mt-1" onClick = "fetchFile('chat1.php')">Connect</h5>-->
				<h5><a class="nav-link text-white mt-1" href="chat1.php">Connect</a></h5>
			</li>
                  <li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white logout" onClick = "fetchFile('login_page.html')">Contact us</h5>-->
				<h5><a class="nav-link text-white logout" href="login_page.html">Contact us</a></h5>
			</li>
            <li class="nav-item mr-4">
				<a href="login_page.html"><abbr title="Logout"><i class="fas fa-user-circle icon"></abbr></i></a>
			</li>
		</ul>
	</div>
      </nav>
    <div class="mt-3 p-3 mx-auto" style="max-width: 600px;">
    <div class="input-group search">
        <div class="form-outline">
        <input id="myInput" type="search" id="form1" class="form-control" placeholder="Search here"/>
        </div>
        <button id="search-button" type="button" class="btn" style="background:rgba(0,0,0,0.9); color: white;">
        <i class="fas fa-search"></i>
        </button>
        <a href="video2.php"><input type ="submit" id="button" class="btn butt" value="Upload" /></a>
    </div>
    </div>
</div>

<!--<script>
    let link1 = document.querySelector("#link1");
    let link2 = document.querySelector('#link2');
    let link3 = document.querySelector('#link3');

    link1.addEventListener("click",fetchurl("file.php"));
    link2.addEventListener("click",fetchurl("chat.php"));
    link2.addEventListener("click",fetchurl("video1.php"));
    function fetchurl(url)
    {
        var xhttp= new XMLHttpRequest();
	    xhttp.onreadystatechange=function(){
		    if(this.readyState==4 && this.status==200){
			    document.getElementById("demo").innerHTML=this.responseText;
			}
		};
	xhttp.open("GET", url, true); 
	xhttp.send();
    }

</script>-->



<?php
    $connect = new mysqli("localhost", "root", "", "trail") or die("Unable to connect database");
    $sql = "select *from videos where category ='knowledge'";
    $result = $connect->query($sql);
    echo "<div class='container-fluid'>";
    echo "<h3 class='subheading'>Videos related to knowledge</h3>";
    if($result->num_rows>0)
    {
        echo "<div class='row'>";
        while($row=$result->fetch_assoc())
		{
   			$location = $row['url'];
   			$name = $row['name'];
            $about = $row['about'];

            echo "<div class='col-sm-3 video' id = 'myId'>
	 			<video src= '$location'  controls width='100%' height='300px' ></video>     
	 			<br>
	  			<center><span id='mdr'>'$name'</span>
	  			<p>$about</p></center>
			   </div>";
        }
        echo "</div>";
    }
    else
    {
        echo "<p>No videos found";
    }

    $sql = "select *from videos where category ='inspiration'";
    $result = $connect->query($sql);
    echo "<h3 class='subheading'>Inspirational videos</h3>";
    if($result->num_rows>0)
    {
        echo "<div class='row'>";
        while($row=$result->fetch_assoc())
        {
            $location = $row['url'];
            $name = $row['name'];
            $about = $row['about'];
   
            echo "<div class='col-sm-4 col-md-3 video' id = 'myId'>
                <video src= '$location'  controls width='100%' height='300px' ></video>     
                <br>
                <center><span id='mdr'>'$name'</span>
                <p>$about</p></center>
                </div>";
        }
        echo "</div>";
        }
        else
        {
           echo "<p>No videos found";
        }

        $sql = "select *from videos where category ='fest'";
        $result = $connect->query($sql);
        echo "<h3 class='subheading'>Videos related to fest</h3>";
        if($result->num_rows>0)
        {
            echo "<div class='row'>";
            while($row=$result->fetch_assoc())
            {
                $location = $row['url'];
                $name = $row['name'];
                $about = $row['about'];
   
                echo "<div class='col-sm-4 col-md-3 video' id = 'myId'>
                <video src= '$location'  controls width='100%' height='300px' ></video>     
                <br>
                <center><span id='mdr'>'$name'</span>
                <p>$about</p></center>
                </div>";
            }
            echo "</div>";
        }
        else
        {
           echo "<p>No videos found";
        }

        $sql = "select *from videos where category ='others'";
        $result = $connect->query($sql);
        echo "<h3 class='subheading'>Other videos</h3>";
        if($result->num_rows>0)
        {
            echo "<div class='row'>";
            while($row=$result->fetch_assoc())
            {
                $location = $row['url'];
                $name = $row['name'];
                $about = $row['about'];
   
                echo "<div class='col-sm-4 col-md-3 video' id='myId'>
                <video src= '$location'  controls width='100%' height='300px' ></video>     
                <br>
                <center><span id='mdr'>'$name'</span>
                <p>$about</p></center>
                </div>";
            }
            echo "</div>";
        }
        else
        {
           echo "<p>No videos found</p>";
        }
        echo "</div>";
	
 ?>
</div>


</body>
</html>