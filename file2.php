<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="preconnect" href="https://fonts.gstatic.com"> 
	<link href="https://fonts.googleapis.com/css2?family=Acme&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-		    UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<style>

    *{
        box-sizing: border-box;
    }
   
    body{
       background: #F8F8F8;
    }

     .butt{
         margin-left: 50px;
         border: 1px solid #12232E;
     }

     .nav2{
         margin-top: 10px;
         
     }


     .about{
         font-weight: bold;
     }

     .subheading{
        margin-top: 10px;
        color: #12232E;
        text-decoration: underline; 
     }

     span a:link{
         color: black;
     }
     span a:visited{
         color: black;
     }
     span a:hover{
         color: #007CC7;
        
     }


     #heading{
        color: white;
        font-family:'Acme', sans-serif;
    }

    .logout{
        border: 1px solid white;
        border-radius: 5px;
    }

    .cards{
        display: grid;
        grid-template-columns: 1fr 1fr 1fr;
        margin-bottom : 2em;
    }

    ul{
        list-style-type: none;
    }

    .one, .two, .three, .four, .five, .six, .seven, .eight, .nine{
        border : 1px solid lightgray;
        border-radius :15px;
        margin: 10px;
        padding: 15px;
        margin-left: 3em;
        margin-right: 3em;
        overflow: auto;
        height: 250px;
    }

    /*.cards ul li {
        padding-left: 2rem;
        background-image: url(arrow.png);
        background-position: 0 0;
        background-size: 2rem 1.6rem;
        background-repeat: no-repeat;
    }*/

    h4{
        margin-left: 1em;
        padding-bottom: 0.5em;
        font-family: 'Acme', sans-serif;
    }
    .icon{
			color: white;
			margin: 5px;
			font-size: 35px;
			cursor: pointer;
		}

    @media screen and (max-width: 45em){
		.cards{
    			grid-template-columns:100%;

		}
    }


</style>
</head>
<body>
<div class="contanier-fluid">
<nav class="navbar navbar-expand-sm  navbar-dark sticky-top py-1 rounded nav " style=" border: 1px ridge gainsboro;background-color:rgba(0,0,0,0.9)">
	<h2 id="heading">RGUKT students Academic knowledge Hub</h2>
	<button class="navbar-toggler" data-toggle="collapse" data-target="#links">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="links">
		<ul class="navbar-nav ml-auto nav">
                  <li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white mt-1">Home</h5>-->
				<h5><a class="nav-link text-white mt-1" href="homepage.php">Home</a></h5>
			</li>
			<li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white mt-1"  onClick = "fetchFile('video1.php')">Videos</h5>-->
				<h5><a class="nav-link text-white mt-1" href="video1.php">Videos</a></h5>
			</li>
			<li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white mt-1"  onClick = "fetchFile('file2.php')">Files</h5>-->
				<h5><a class="nav-link text-white mt-1" href="#">Files</a></h5>
                  </li>
                  <li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white mt-1" onClick = "fetchFile('chat1.php')">Connect</h5>-->
				<h5><a class="nav-link text-white mt-1" href="ideas.php">Ideas</a></h5>
			</li>
                  <li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white mt-1" onClick = "fetchFile('chat1.php')">Connect</h5>-->
				<h5><a class="nav-link text-white mt-1" href="chat1.php">Connect</a></h5>
			</li>
                  <li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white logout" onClick = "fetchFile('login_page.html')">Contact us</h5>-->
				<h5><a class="nav-link text-white logout" href="login_page.html">Contact us</a></h5>
			</li>
            <li class="nav-item mr-4">
				<a href="login_page.html"><abbr title="Logout"><i class="fas fa-user-circle icon"></abbr></i></a>
			</li>
		</ul>
	</div>
      </nav>
     <div id="demo">
    </nav>
    </nav>
    <div class="mt-3 p-3 mx-auto" style="max-width: 430px;">
    <div class="input-group search">
        <div class="form-outline">
        <input id="myInput" type="search" id="form1" class="form-control" placeholder="Search here"/>
        </div>
        <button id="search-button" type="button" class="btn" style="background: #12232E; color: white;">
        <i class="fas fa-search"></i>
        </button>
        <a href="file3.php"><input type ="submit" id="button" class="btn butt" value="Upload" /></a>
    </div> 
    </div>
    </div>
    <div class="contanier-fluid">
        <div class="cards">
            <div class="one">
                <h4>General books</h4>
                <ul class="oneul">
                </ul>
            </div>
            <div class="two">
                <h4>PUC-1 content files</h4>
                <ul class="twoul">
                </ul>
            </div>
            <div class="three">
                <h4>PUC-2 content files</h4>
                <ul class="threeul">
                </ul>
            </div>
            <div class="four">
                <h4>E1 content files</h4>
                <ul class="fourul">
                </ul>
            </div>
            <div class="five">
                <h4>E2 content files</h4>
                <ul class="fiveul">
                </ul>
            </div>
            <div class="six">
                <h4>E3 content files</h4>
                <ul class="sixul">
                </ul>
            </div>
            <div class="seven">
                <h4>E4 content files</h4>
                <ul class="sevenul">
                </ul>
            </div>
            <div class="eight">
                <h4>Competitive files</h4>
                <ul class="eightul">
                </ul>
            </div>
            <div class="nine">
                <h4>Other files</h4>
                <ul class="nineul">
                </ul>
            </div>
    </div>



<?php
    /*$connect = new mysqli("localhost", "root", "", "trail") or die("Unable to connect database");
    $sql = "select *from files where category ='content'";
    $result = $connect->query($sql);
    echo "<div class='container-fluid'>";
    echo "<div class='border border-muted mt-1 p-3 mx-auto ' style='max-width: 400px;'>";
    echo "<h4 class='subheading'>Content files</h4>";
    if($result->num_rows>0)
    {
        while($row=$result->fetch_assoc())
		{
   			$location = $row['url'];
   			$name = $row['name'];
            $about = $row['about'];

            echo "<div class='card'><span><a href='$location'>$name</a></span>
                <p><span class='about'>About: </span>$about</p>
                </div>";
        };
    }
    else
    {
        echo "<p>No files found</p>";
    }

    $sql = "select *from files where category ='general_books'";
    $result = $connect->query($sql);
    echo "<h4 class='subheading'>General books</h4>";
    if($result->num_rows>0)
    {
        while($row=$result->fetch_assoc())
        {
            $location = $row['url'];
            $name = $row['name'];
            $about = $row['about'];
   
            echo "<div class='card'><span><a href='$location'>$name</a></span>
            <p><span class='about'>About: </span>$about</p>
            </div>";
        }
        }
        else
        {
           echo "<p>No files found";
        }

        $sql = "select *from files where category ='others'";
        $result = $connect->query($sql);
        echo "<h4 class='subheading'>Other files</h4>";
        if($result->num_rows>0)
        {
            while($row=$result->fetch_assoc())
            {
                $location = $row['url'];
                $name = $row['name'];
                $about = $row['about'];
   
                echo "<div class='card'><span><a href='$location'>$name</a></span>
                <p><span class='about'>About: </span>$about</p>
                </div>";
            }
        }
        else
        {
           echo "<p>No files found";
        }
        echo "</div></div>";*/
        $connect = new mysqli("localhost", "root", "", "trail") or die("Unable to connect database");
        $sql = "select *from files";
        $result = $connect->query($sql);
        if($result->num_rows>0)
        {
            while($row=$result->fetch_assoc())
            {
                $location = $row['url'];
                $name = $row['name'];
                $about = $row['about'];
                

                if($row['category']=="general_books")
                {
                ?>
                    <script>
                    var node = document.createElement("li");
                    node.innerHTML = "<span><a href= <?php echo $location ?> > <?php echo $name ?> </a> </span><p><span class='about'>About: </span> <?php echo $about ?></p>"
                    document.querySelector(".oneul").appendChild(node);
                    </script>
                <?php
                }
                if($row['category']=="p1")
                {
                ?>
                    <script>
                    var node = document.createElement("li");
                    node.innerHTML = "<span><a href= <?php echo $location ?> > <?php echo $name ?> </a> </span><p><span class='about'>About: </span> <?php echo $about ?></p>"
                    document.querySelector(".twoul").appendChild(node);
                    </script>
                <?php
                }

                if($row['category']=="p2")
                {
                ?>
                    <script>
                    var node = document.createElement("li");
                    node.innerHTML = "<span><a href= <?php echo $location ?> > <?php echo $name ?> </a> </span><p><span class='about'>About: </span> <?php echo $about ?></p>"
                    document.querySelector(".threeul").appendChild(node);
                    </script>
                <?php
                }
                if($row['category']=="e1")
                {
                ?>
                    <script>
                    var node = document.createElement("li");
                    node.innerHTML = "<span><a href= <?php echo $location ?> > <?php echo $name ?> </a> </span><p><span class='about'>About: </span> <?php echo $about ?></p>"
                    document.querySelector(".fourul").appendChild(node);
                    </script>
                <?php
                }
                if($row['category']=="e2")
                {
                ?>
                    <script>
                    var node = document.createElement("li");
                    node.innerHTML = "<span><a href= <?php echo $location ?> > <?php echo $name ?> </a> </span><p><span class='about'>About: </span> <?php echo $about ?></p>"
                    document.querySelector(".fiveul").appendChild(node);
                    </script>
                <?php
                }
                if($row['category']=="e3")
                {
                ?>
                    <script>
                    var node = document.createElement("li");
                    node.innerHTML = "<span><a href= <?php echo $location ?> > <?php echo $name ?> </a> </span><p><span class='about'>About: </span> <?php echo $about ?></p>"
                    document.querySelector(".sixul").appendChild(node);
                    </script>
                <?php
                }
                if($row['category']=="e4")
                {
                ?>
                    <script>
                    var node = document.createElement("li");
                    node.innerHTML = "<span><a href= <?php echo $location ?> > <?php echo $name ?> </a> </span><p><span class='about'>About: </span> <?php echo $about ?></p>"
                    document.querySelector(".sevenul").appendChild(node);
                    </script>
                <?php
                }
                if($row['category']=="competitive")
                {
                ?>
                    <script>
                    var node = document.createElement("li");
                    node.innerHTML = "<span><a href= <?php echo $location ?> > <?php echo $name ?> </a> </span><p><span class='about'>About: </span> <?php echo $about ?></p>"
                    document.querySelector(".eightul").appendChild(node);
                    </script>
                <?php
                }
                if($row['category']=="others")
                {
                ?>
                    <script>
                    var node = document.createElement("li");
                    node.innerHTML = "<span><a href= <?php echo $location ?> > <?php echo $name ?> </a> </span><p><span class='about'>About: </span> <?php echo $about ?></p>"
                    document.querySelector(".nineul").appendChild(node);
                    </script>
                <?php
                }

                
            }
        }
	
 ?>




<script>
    console.log("ok");
    $(document).ready(function(){
        $("#myInput").on("keyup", function() {
            //console.log($(this).val());
            value = $(this).val().toLowerCase();
            $("span a").filter(function() {
                //console.log($(this));
                console.log($(this).text());
                $(this).parents('li').toggle($(this).text().toLowerCase().indexOf(value) >-1);
            });
        });
    });
</script>
</body>
</html>