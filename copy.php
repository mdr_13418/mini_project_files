<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link rel="preconnect" href="https://fonts.gstatic.com"> 
	<link href="https://fonts.googleapis.com/css2?family=Acme&display=swap" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
	<style>	
		
		.cards{
			display: grid;
			grid-template-columns: 35% 65%;
		}
		.card1, .card2 {
			border : 1px solid lightgray;
			height : 530px;
			overflow : auto;
            }
		
		.head{
			font-family: 'Acme', sans-serif;
			position: relative;
			font-weight: 400;
			font-size: 2.3rem;
			margin : 10px;
			color: #333366;
		}

		h3{
			padding: 1em 0px;
			text-align: center;
		}

		.card1{
			margin-left: 2em;
			padding-left: 2em;
		}
		#post{
			margin-left:15em;
			width: 25%;
		}



		ul li .mdr{
			cursor : pointer;
			font-size : 20px;
		}

		ul li .mdr:hover{
			text-decoration : underline;
			color: #4481eb;
		}
		.about{
			margin-left : 10em;
		}

		.card2{
			padding : 2em;
		}

		.about1{
			margin-left : 30em;
		}
		#btn{
			margin-left: 28em;
			width : 20%;
			background-color : #4481eb;
			color : white;
		}

		#heading{
        		color: white;
       		font-family:'Acme', sans-serif;
    		}

    		.logout{
        		border: 1px solid white;
        		border-radius: 5px;
    		}

		.icon{
			color: white;
			margin: 5px;
			font-size: 35px;
			cursor: pointer;
		}
	</style>
</head>
<body>
<div class="container-fluid">
<nav class="navbar navbar-expand-sm  navbar-dark sticky-top py-1 rounded nav " style=" border: 1px ridge gainsboro;background-color:rgba(0,0,0,0.9)">
	<h2 id="heading">RGUKT students Academic knowledge Hub</h2>
	<button class="navbar-toggler" data-toggle="collapse" data-target="#links">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="links">
		<ul class="navbar-nav ml-auto nav">
                  <li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white mt-1">Home</h5>-->
				<h5><a class="nav-link text-white mt-1" href="homepage.php">Home</a></h5>
			</li>
			<li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white mt-1"  onClick = "fetchFile('video1.php')">Videos</h5>-->
				<h5><a class="nav-link text-white mt-1" href="video1.php">Videos</a></h5>
			</li>
			<li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white mt-1"  onClick = "fetchFile('file2.php')">Files</h5>-->
				<h5><a class="nav-link text-white mt-1" href="#">Files</a></h5>
                  </li>
                  <li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white mt-1" onClick = "fetchFile('chat1.php')">Connect</h5>-->
				<h5><a class="nav-link text-white mt-1" href="chat1.php">Connect</a></h5>
			</li>
                  <li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white logout" onClick = "fetchFile('login_page.html')">Contact us</h5>-->
				<h5><a class="nav-link text-white logout" href="login_page.html">Contact us</a></h5>
			</li>
            <li class="nav-item mr-4">
				<a href="login_page.html"><abbr title="Logout"><i class="fas fa-user-circle icon"></abbr></i></a>
			</li>
		</ul>
	</div>
      </nav>

      <div class="cards">
            <div class="card1">
                  <h3>Post your Ideas/Problems</h3>
                  <form action="ideas.php" id="usrform" method = "POST">
                  <label for = "usrname">From:</label>
                  <input type="text" name="usrname" id = "usrname">
                  <br>
			<label for="comment" id="labe">Post here your idea/problem</label><br>
                  <textarea rows="4" cols="35" name="comment" id="comment" form="usrform"></textarea>
			<br>
                  <input type="submit" value="Post" name="submit" id="post" style="background: #4481eb; color: white" ><br>
                  </form>
			<br>

			<div class="trail">
				<h4 style="font-family:'Acme', sans-serif">Posted Ideas & Problems</h4>
				<ul class="trailul">
				</ul>
			</div>
            </div>
            <div class="card2">
		    <h2 id = "headingg"></h2>
               <div id = "demo"></div>  
		   <div id = "demo1"></div>  
            </div>
      </div>
</div>
<!--<script>
	let input = document.querySelector("#post");
	input.addEventListener("click", () => {
		let value = document.querySelector("#usrname").value;
		let comment = document.querySelector("#comment").value;
		var node = document.createElement("li");
		node.innerHTML = `<span>${comment}</span><p><span class='about'>By: ${value}</p>`;
		document.querySelector(".trailul").appendChild(node);

	})
</script>-->

<?php

$connect = new mysqli("localhost", "root", "", "trail") or die("Unable to connect database");

if(isset($_POST['submit']))
{
	$name = $_POST["usrname"];
	$idea = $_POST["comment"];

	$newrow="insert into ideas(name,idea) values('$name','$idea')";
		if($connect->query($newrow))
		{
			?>
			<script>
			console.log("Ok");
			location.replace("ideas.php");
			</script>
			<?php
		}
		else
			echo "Error in creating record:" .$connect->error;
}


$sql = "select *from ideas";
$result = $connect->query($sql);
if($result->num_rows>0)
{
    while($row=$result->fetch_assoc())
    {
	  $name = $row['name'];
	  $idea = $row['idea'];
	  ?>
		<script>
		var node = document.createElement("li");
		node.innerHTML = "<div class='idea'><span class='mdr' onClick='change(this)'><?php echo $idea ?></span><p><span class='about'>By: </span> <?php echo $name ?></p></div>"
		document.querySelector(".trailul").appendChild(node);
		</script>
	<?php
    }
}
?>
<script>
	let divs = document.querySelector('.mdr');
	var ide;
	function change(e){
		ide = e.innerHTML;
		let ele = document.createElement("div");
		//let ele1 = document.createElement("h2");
		document.querySelector('#headingg').innerHTML = e.innerHTML;
		let ele2 = document.createElement("div");
		ele2.innerHTML=  "<p>Comment here</p><textarea rows='7' cols='70' name='comment1' id='comment1'></textarea><br>From:<input type='text' id ='by'><br>";
		let ele3 = document.createElement("button");
		let txt = document.createTextNode("Post");
		ele3.appendChild(txt);
		ele3.setAttribute("id","btn");
		//ele1.innerHTML = e.innerHTML;
		//ele.appendChild(ele1);
		ele.appendChild(ele2);
		ele.appendChild(ele3);
		document.querySelector('#demo1').innerHTML = ele.innerHTML;	
		
		document.querySelector('#btn').addEventListener("click",() => {
			let ee = document.querySelector('#comment1').value;
			let p = document.querySelector('#by').value;
			document.querySelector('#comment1').value="";
			document.querySelector('#by').value="";
			if((ee!="")&&(p!=""))
			{
				let xmlhttp = new XMLHttpRequest();
    				xmlhttp.onreadystatechange = function() {
      			if(this.readyState == 4 && this.status == 200) {
					let ele7 = document.createElement("div");
					ele7.innerHTML = `<div><span>${ee}</a></span><p><span class='about1'>From: </span>${p}</p></div>`;
					document.querySelector("#demo").appendChild(ele7);
					console.log("This is message: "+ this.responseText);
      			}
    				}
    				xmlhttp.open("POST","ideas1.php",true);
				xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    				xmlhttp.send("idea="+ide+"&reply="+ee+"&name="+p);
				console.log(ide);
			}
		});

			
		let xh = new XMLHttpRequest();
    		xh.onreadystatechange = function() {
      	if(this.readyState == 4 && this.status == 200) {
			//console.log("This is message: "+ this.responseText);
			document.querySelector("#demo").innerHTML = this.responseText;
      		}
		}
    		xh.open("POST","ideas2.php",true);
		xh.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    		xh.send("idea="+ide);
				//console.log(ide);
			
		
	}	
</script>

</body>
</html>
