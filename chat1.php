<?php
if (isset($_POST['submit'])){
/* Attempt MySQL server connection. Assuming
you are running MySQL server with default 
setting (user 'root' with no password) */
$link = mysqli_connect("localhost", 
            "root", "", "trail");
   
// Check connection
if($link === false){
    die("ERROR: Could not connect. " 
          . mysqli_connect_error());
}
   
// Escape user inputs for security
$un= mysqli_real_escape_string(
      $link, $_REQUEST['uname']);
$m = mysqli_real_escape_string(
      $link, $_REQUEST['msg']);
date_default_timezone_set('Asia/Kolkata');
$ts=date('y-m-d h:ia');
   
// Attempt insert query execution
$sql = "INSERT INTO chats (uname, msg, dt)
        VALUES ('$un', '$m', '$ts')";
if(mysqli_query($link, $sql)){
    ;
} else{
    echo "ERROR: Message not sent!!!";
}
 // Close connection
mysqli_close($link);
}
?>
<html>
<head>

    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="preconnect" href="https://fonts.gstatic.com"> 
	<link href="https://fonts.googleapis.com/css2?family=Acme&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<style>
    *{
        box-sizing:border-box;
    }

    body{
        font-family:Arial;
        background-color: #F0F0F0;
    }

    #heading{
        font-family:'Acme', sans-serif;
    }

    .logout{
        border: 1px solid white;
        border-radius: 5px;
    }


#container{
   
    width:500px;
    height:700px;
    background:white;
    margin:0 auto;
    margin-top: 10px;
    font-size:0;
    border-radius:5px;
    overflow:hidden;
}
main{
    width:500px;
    height:700px;
    display:inline-block;
    font-size:15px;
    vertical-align:top;
}
main header{
    height:80px;
    padding:30px 20px 30px 40px;
    background-color: rgba(0,0,0,0.7);   
}
main header > *{
    display:inline-block;
    vertical-align:top;
}

main header div{
    margin-left:120px;
    margin-right:90px;
}
main header h2{
    font-size:25px;
    margin-top:2px;
    text-align:center;
    color:#FFFFFF;   
}
main .inner_div{
    padding-left:0;
    margin:0;
    list-style-type:none;
    position:relative;
    overflow:auto;
    height:500px;
    background-image:url(bg2.png);
    background-position:center;
    background-repeat:no-repeat;
    background-size:cover;
    position: relative;
    border-top:2px solid #fff;
    border-bottom:2px solid #fff;
      
}
main .triangle{
    width: 0;
    height: 0;
    border-style: solid;
    border-width: 0 8px 8px 8px;
    border-color: transparent transparent
      #58b666 transparent;
    margin-left:20px;
    clear:both;
}
main .message{
    padding:10px;
    color:#000;
    margin-left:15px;
    background-color:#58b666;
    line-height:20px;
    max-width:90%;
    display:inline-block;
    text-align:left;
    border-radius:5px;
    clear:both;
}
main .triangle1{
    width: 0;
    height: 0;
    border-style: solid;
    border-width: 0 8px 8px 8px;
    border-color: transparent 
      transparent #6fbced transparent;
    margin-right:20px;
    float:right;
    clear:both;
}
main .message1{
    padding:10px;
    color:#000;
    margin-right:15px;
    background-color:#6fbced;
    line-height:20px;
    max-width:90%;
    display:inline-block;
    text-align:left;
    border-radius:5px;
    float:right;
    clear:both;
}
  
main footer{
    height:150px;
    padding:20px 30px 10px 20px;
    background-color: rgba(0,0,0,0.7);
}
main footer .input1{
    resize:none;
    border:100%;
    display:block;
    width:120%;
    height:55px;
    border-radius:3px;
    padding:20px;
    font-size:13px;
    margin-bottom:13px;
}
main footer textarea{
    resize:none;
    border:100%;
    display:block;
    width:140%;
    height:55px;
    border-radius:3px;
    padding:20px;
    font-size:13px;
    margin-bottom:13px;
    margin-left:20px;
}
main footer .input2{
    resize:none;
    border:100%;
    display:block;
    width:40%;
    height:55px;
    border-radius:3px;
    padding:20px;
    font-size:13px;
    margin-bottom:13px;
    margin-left:100px;
    color:white;
    text-align:center;
    background-color:#12232E;
    border: 2px solid white;  
}
}
main footer textarea::placeholder{
    color:#ddd;
}

#heading{
        color: white;
    }
    .icon{
			color: white;
			margin: 5px;
			font-size: 35px;
			cursor: pointer;
		}
  
</style>
<body onload="show_func()">
<div class="contanier-fluid">

<nav class="navbar navbar-expand-sm  navbar-dark sticky-top py-1 rounded nav " style=" border: 1px ridge gainsboro;background-color:rgba(0,0,0,0.9)">
	<h2 id="heading">RGUKT students Academic knowledge Hub</h2>
	<button class="navbar-toggler" data-toggle="collapse" data-target="#links">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="links">
		<ul class="navbar-nav ml-auto nav">
                  <li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white mt-1">Home</h5>-->
				<h5><a class="nav-link text-white mt-1" href="homepage.php">Home</a></h5>
			</li>
			<li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white mt-1"  onClick = "fetchFile('video1.php')">Videos</h5>-->
				<h5><a class="nav-link text-white mt-1" href="video1.php">Videos</a></h5>
			</li>
			<li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white mt-1"  onClick = "fetchFile('file2.php')">Files</h5>-->
				<h5><a class="nav-link text-white mt-1" href="file2.php">Files</a></h5>
                  </li>
                  <li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white mt-1" onClick = "fetchFile('chat1.php')">Connect</h5>-->
				<h5><a class="nav-link text-white mt-1" href="ideas.php">Ideas</a></h5>
			</li>
                  <li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white mt-1" onClick = "fetchFile('chat1.php')">Connect</h5>-->
				<h5><a class="nav-link text-white mt-1" href="#">Connect</a></h5>
			</li>
                  <li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white logout" onClick = "fetchFile('login_page.html')">Contact us</h5>-->
				<h5><a class="nav-link text-white logout" href="login_page.html">Contact us</a></h5>
			</li>
            <li class="nav-item mr-4">
				<a href="login_page.html"><abbr title="Logout"><i class="fas fa-user-circle icon"></abbr></i></a>
			</li>
		</ul>
	</div>
      </nav>
     <div id="demo">
    </nav>
    </nav>
</div>
<div id="container">
    <main>
        <header>
            <div>
                <h2 id="headingg">Connect here</h2>
            </div>
        </header>
  
<script>
function show_func(){
  
 var element = document.getElementById("chathist");
    element.scrollTop = element.scrollHeight;
   
 }
 </script>
   
<form id="myform" action="chat1.php" method="POST" >
<div class="inner_div" id="chathist">
<?php 
$host = "localhost"; 
$user = "root"; 
$pass = ""; 
$db_name = "trail"; 
$con = new mysqli($host, $user, $pass, $db_name);
  
$query = "SELECT * FROM chats";
 $run = $con->query($query); 
 $i=0;
   
 while($row = $run->fetch_array()) : 
 if($i==0){
 $i=5;
 $first=$row;
 ?>
 <div id="triangle1" class="triangle1"></div>
 <div id="message1" class="message1"> 
 <span style="color:white;float:right;">
 <?php echo $row['msg']; ?></span> <br/>
 <div>
   <span style="color:black;float:left;
   font-size:10px;clear:both;">
    <?php echo $row['uname']; ?>, 
        <?php echo $row['dt']; ?>
   </span>
</div>
</div>
<br/><br/>
 <?php
 }
else
{
if($row['uname']!=$first['uname'])
{
?>
 <div id="triangle" class="triangle"></div>
 <div id="message" class="message"> 
 <span style="color:white;float:left;">
   <?php echo $row['msg']; ?>
 </span> <br/>
 <div>
  <span style="color:black;float:right;
          font-size:10px;clear:both;">
  <?php echo $row['uname']; ?>, 
        <?php echo $row['dt']; ?>
 </span>
</div>
</div>
<br/><br/>
<?php
} 
else
{
?>
 <div id="triangle1" class="triangle1"></div>
 <div id="message1" class="message1"> 
 <span style="color:white;float:right;">
  <?php echo $row['msg']; ?>
 </span> <br/>
 <div>
 <span style="color:black;float:left;
         font-size:10px;clear:both;"> 
 <?php echo $row['uname']; ?>, 
      <?php echo $row['dt']; ?>
 </span>
</div>
</div>
<br/><br/>
<?php
}
}
endwhile;
?>
</div>
    <footer>
        <table>
        <tr>
           <th>
            <input  class="input1" type="text" 
                    id="uname" name="uname" 
                    placeholder="From">
           </th>
           <th>
            <textarea id="msg" name="msg" 
                rows='3' cols='50'
                placeholder="Type your message">
            </textarea></th>
           <th>
            <input class="input2" type="submit" 
            id="submit" name="submit" value="send">
           </th>                
        </tr>
        </table>                
    </footer>
</form>
</main>    
</div>
  
</body>
</html>