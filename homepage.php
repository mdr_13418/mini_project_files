<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link rel="preconnect" href="https://fonts.gstatic.com"> 
	<link href="https://fonts.googleapis.com/css2?family=Acme&display=swap" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
	<style>
		@import url('https://fonts.googleapis.com/css2?family=Noto+Sans+JP&display=swap');

		*{
			box-sizing: border-box;
		}
           body{
		     background-color: #E8E8E8;
		     overflow-x: hidden;
		     overflow-y: auto;
	     }


            .logout{
                  border: 1px solid white;
                  border-radius: 5px;
            }

            #heading{
                  color: white;
                  font-family:'Acme', sans-serif;
			font-weight: normal;
            }

           .row{
		      
                  background-image: url("threee.jpg");
                  background-repeat: no-repeat;
                  background-size:cover;
                  height: 280px;
                  background-attachment:fixed;
                  overflow:hidden;
			z-index: 1;
            }

		.content{
			position: absolute;
			color: white;
			z-index:2;
			left: 15%;
			top: 25%;
		}
		.head{
			font-family: 'Noto Sans JP', sans-serif;
			font-size: 40px;
		}

		.body{
			font-size: 20px;
		}

		.nav{
			overflow:hidden;
		}

		.cards{
			display:grid;
			grid-template-columns: 1fr 1fr 1fr;
		}

		.card1, .card2, .card3{
			font-family: 'Noto Sans JP', sans-serif;
			color: black;
			padding : 15px;
			margin-top: 2em;
			line-height: 1em;
		}

		h5{
      		cursor : pointer;   
     		}
		     .count{
			background :rgba(0,0,0,0.9);
			color: white;
			padding: 10px;
			border: 1px solid lightgray;
			border-radius: 15px;
		}

		.icon{
			color: white;
			margin: 5px;
			font-size: 35px;
			cursor: pointer;
		}

		.card3{
			line-height: 1.5em;
			word-spacing: 0.7em;
			height : 180px;
			overflow: hidden;
		}

		 @media screen and (max-width: 45em){
		.cards{
    			grid-template-columns:100%;
		}

		 }
		 
      </style>
</head>
<body>
<div class="contanier-fluid">
	<nav class="navbar navbar-expand-sm  navbar-dark sticky-top py-1 rounded nav " style=" border: 1px ridge gainsboro;background-color:rgba(0,0,0,0.9)">
	<h2 id="heading">RGUKT students Academic knowledge Hub</h2>
	<button class="navbar-toggler" data-toggle="collapse" data-target="#links">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="links">
		<ul class="navbar-nav ml-auto nav">
                  <li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white mt-1">Home</h5>-->
				<h5><a class="nav-link text-white mt-1" href="#">Home</a></h5>
			</li>
			<li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white mt-1"  onClick = "fetchFile('video1.php')">Videos</h5>-->
				<h5><a class="nav-link text-white mt-1" href="video1.php">Videos</a></h5>
			</li>
			<li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white mt-1"  onClick = "fetchFile('file2.php')">Files</h5>-->
				<h5><a class="nav-link text-white mt-1" href="file2.php">Files</a></h5>
                  </li>
			<li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white mt-1" onClick = "fetchFile('chat1.php')">Connect</h5>-->
				<h5><a class="nav-link text-white mt-1" href="ideas.php">Ideas</a></h5>
			</li>
                  <li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white mt-1" onClick = "fetchFile('chat1.php')">Connect</h5>-->
				<h5><a class="nav-link text-white mt-1" href="chat1.php">Connect</a></h5>
			</li>
                  <li class="nav-item mr-4">
				<!--<h5 class="nav-link text-white logout" onClick = "fetchFile('login_page.html')">Contact us</h5>-->
				<h5><a class="nav-link text-white logout" href="login_page.html">Contact us</a></h5>
			</li>
			<li class="nav-item mr-4">
				<a href="login_page.html"><abbr title="Logout"><i class="fas fa-user-circle icon"></abbr></i></a>
			</li>
		</ul>
	</div>
      </nav>
	</div>
	<div id="demo">
	<div class="container-fluid">

    <div class="image row">
          <div class="col-sm-12 content">
	    <h2 class="head">College Connect to get connect</h2><br>
	    <span class="body">A platform to engage students by sharing resources, ideas and media...</span>
	    <p class="body">Come on! Join with us and have fun..!</p>
          </div>
    </div>
	</div>

	<div class='container'>
	<div class="cards">
		<div class="card1">
			<h3>Objectives</h3><br>
			<ul style="line-height:1.5em">
			<li>Sharing Resources</li>
			<li>Collecting Media</li>
			<li>Engaging with College</li>
			</ul>
		</div>
		<div class="card2">
			<h3>Why this Platform?</h3><br>
			<ul>
			<p>It's easy to connect with college students</p>
			<p>To connect students of similar interests</p>
			<p>We can collect all content,updates related to college in one platform</p>
			</ul>
		</div>
		<div class="card3">
			<h3>Updates</h3>
			<marquee behavior="scroll" scrollamount= "5" scrolldelay="2" direction="up" onmouseover="this.stop();" onmouseout="this.start();" class="notification-box">
			<div>
			<p>E3 Mid-01 was postponed, schedule will come soon(06-04-2021)</p>
			<p>students were fight for their rights(17-03-2021)</p>
			<p>Sealed quotations from the registered firms/suppliers for Pen Tablet without display for Rajiv Gandhi University of Knowledge Technologies - A.P, Ongole Campus.(26-03-2021)</p>	
			Sealed quotations are hereby invited from the registered firms/suppliersfor LED Tube Lights 20Wfor RGUKT-AP, Ongole Campus located at Idupulapaya, Vempalli, YSR District.(22-03-2021)</p>
			<p>sealed quotations are hereby invited from the registered firms/suppliers for Ceiling Fans for RGUKT-AP, Ongole Campus, BesideSanghamitra Hospital,Near South Bypass NH-5, Ongole, Prakasam District.(29-03-2021)</p>

			</div>
			</marquee>
		</div>
	</div>
</div>
</div>

<?php



  $connect = new mysqli("localhost", "root", "", "trail") or die("Unable to connect database");

  $sql_call = "update usercount set no = no + 1"; 
  mysqli_query($connect, $sql_call);
  $sql="select * from usercount";
  $result = $connect->query($sql);
  $row=$result->fetch_assoc();
  $mdr = $row["no"];

  echo "<center><span class='count'>Visited users: $mdr</span></center>";
?>

</body>
</html>	